# Mengenal Anhar

Mengenal Anhar, yaitu mengenal Angka, Huruf, dan Angka.

The current features:
- Mengenal angka 1-10
- Mengenal huruf latin A-Z
- Mengenal warna dasar

What next?
- Mengenal huruf hijaiyyah
- Mengenal kosakata dasar bahasa dan arabic
    * Buah
    * Anggota badan
    * Transportasi
    * Benda sekitar

## Getting to know about the code

This project is a starting point for a Flutter application.

You can jump to `lib > main.dart` to get highlevel of this project
- We have four files on a `pages` folder
- We haev one file on a `widget`folder

The other files that I actively modified are
- pubspec.yaml
- assets folder
- android folder related to release purpose
- ios folder when I try to release in iOS version

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
