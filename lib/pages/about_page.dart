import 'package:flutter/material.dart';

void main() {
  runApp(AboutPage());
}

class AboutPage extends StatelessWidget {
  const AboutPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [
          Container(
            child: Padding(
              padding: const EdgeInsets.all(40),
              child: Column(
                children: [
                  Row(
                    children: [
                      Text(
                        "PETUNJUK PENGGUNAAN",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 17,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 25),
                  RowAbout(Icons.refresh,
                      "Tombol REFRESH untuk mengembalikan \nangka, huruf, dan warna ke posisi awal."),
                  SizedBox(height: 15),
                  RowAbout(Icons.wind_power,
                      "Tombol RANDOM untuk mengacak urutan \nangka, huruf, dan angka."),
                  SizedBox(height: 15),
                  RowAbout(Icons.skip_next,
                      "Tombol NEXT untuk mengganti \nangka, huruf, dan warna ke urutan \nberikutnya."),
                  SizedBox(height: 15),
                  RowAbout(Icons.skip_previous,
                      "Tombol PREV untuk menggannti \nangka, huruf, dan warna ke urutan \nsebelumnya."),
                  SizedBox(height: 35),
                  Row(
                    children: [
                      Text(
                        "TENTANG PEMBELAJARAN",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 17,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 25),
                  RowAbout(Icons.abc, "Pengenalan huruf A sampai Z"),
                  SizedBox(height: 15),
                  RowAbout(Icons.onetwothree_outlined,
                      "Pengenalan angka 1 sampai 10"),
                  SizedBox(height: 15),
                  RowAbout(
                      Icons.palette_outlined, "Pengenalan warna-warna dasar"),
                  SizedBox(height: 15),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

class RowAbout extends StatelessWidget {
  RowAbout(this.ikon, this.teks);

  final String teks;
  final IconData ikon;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Icon(
          ikon,
          color: Colors.teal,
        ),
        SizedBox(width: 9),
        Text(
          teks,
        ),
      ],
    );
  }
}
