import 'dart:math';

import 'package:flutter/material.dart';

class HurufPage extends StatefulWidget {
  @override
  State<HurufPage> createState() => _HurufPageState();
}

class _HurufPageState extends State<HurufPage> {
  // const HurufPage({Key? key}) : super(key: key);
  // int nilai = 0;
  List<String> hurufLatin = [
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "U",
    "V",
    "W",
    "X",
    "Y",
    "Z"
  ];
  int index = 0;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Colors.white,
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text(
                "${hurufLatin[index]}",
                style: TextStyle(fontSize: 240),
              ),
              SizedBox(height: 200),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  OutlinedButton(
                    onPressed: () {
                      index = 0;
                      hurufLatin[index];
                      setState(() {});
                    },
                    child: Icon(Icons.refresh),
                  ),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: Colors.teal,
                    ),
                    onPressed: () {
                      Random random = new Random();
                      int randomNumber = random.nextInt(24);
                      index = randomNumber;
                      hurufLatin[index];
                      setState(() {});
                    },
                    child: Icon(Icons.wind_power),
                  ),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: Colors.teal,
                    ),
                    onPressed: () {
                      if (index == 0) {
                        index = 0;
                        hurufLatin[index];
                      } else {
                        index--;
                        hurufLatin[index];
                      }
                      setState(() {});
                    },
                    child: Icon(Icons.skip_previous),
                  ),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: Colors.teal,
                    ),
                    onPressed: () {
                      if (index >= 25) {
                        index = 0;
                        hurufLatin[index];
                      } else {
                        index++;
                        hurufLatin[index];
                      }
                      setState(() {});
                    },
                    child: Icon(Icons.skip_next),
                  ),
                ],
              ),
              SizedBox(height: 100),
            ],
          ),
        ),
      ),
    );
  }
}
