import 'package:flutter/material.dart';
import 'dart:math';

class AngkaPage extends StatefulWidget {
  @override
  State<AngkaPage> createState() => _AngkaPageState();
}

class _AngkaPageState extends State<AngkaPage> {
  int nilai = 1;
  int nilaiMin = 1;
  int nilaiMax = 10;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Colors.white,
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text(
                "$nilai",
                style: TextStyle(fontSize: 240),
              ),
              SizedBox(height: 200),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  OutlinedButton(
                    onPressed: () {
                      nilai = nilai % nilai;
                      setState(() {});
                    },
                    child: Icon(Icons.refresh),
                  ),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: Colors.teal,
                    ),
                    onPressed: () {
                      Random random = new Random();
                      int randomNumber = random.nextInt(nilaiMax + 1);
                      nilai = randomNumber;
                      setState(() {});
                    },
                    child: Icon(Icons.wind_power),
                  ),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: Colors.teal,
                    ),
                    onPressed: () {
                      if (nilai <= nilaiMin) {
                        nilai = nilaiMax;
                      } else {
                        nilai--;
                      }
                      setState(() {});
                    },
                    child: Icon(Icons.skip_previous),
                  ),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: Colors.teal,
                    ),
                    onPressed: () {
                      if (nilai >= nilaiMax) {
                        nilai = nilaiMin;
                      } else {
                        nilai++;
                      }
                      setState(() {});
                    },
                    child: Icon(Icons.skip_next),
                  ),
                ],
              ),
              SizedBox(height: 100),
            ],
          ),
        ),
      ),
    );
  }
}
