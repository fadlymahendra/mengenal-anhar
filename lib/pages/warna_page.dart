import 'dart:math';

import 'package:flutter/material.dart';

class WarnaPage extends StatefulWidget {
  @override
  State<WarnaPage> createState() => _WarnaPageState();
}

class _WarnaPageState extends State<WarnaPage> {
  List<Color> warna = [
    Colors.red,
    Colors.blue,
    Colors.green,
    Colors.orange,
    Colors.purple,
    Colors.pink,
    Colors.yellow,
    Colors.brown,
    Colors.grey,
    Colors.black87,
    Colors.white,
  ];

  List<String> teksWarna = [
    "Merah",
    "Biru",
    "Hijau",
    "Oranye",
    "Ungu",
    "Pink",
    "Kuning",
    "Coklat",
    "Abu-abu",
    "Hitam",
    "Putih",
  ];

  int index = 0;
  String kelir = "purple";

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Colors.white,
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              KotakWarna(warna[index]),
              SizedBox(height: 20),
              Text(
                teksWarna[index],
                style: TextStyle(
                  color: warna[index],
                ),
              ),
              SizedBox(height: 200),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  OutlinedButton(
                    onPressed: () {
                      index = 0;
                      warna[index];
                      setState(() {});
                    },
                    child: Icon(Icons.refresh),
                  ),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: Colors.teal,
                    ),
                    onPressed: () {
                      Random random = new Random();
                      int randomNumber = random.nextInt(9);
                      index = randomNumber;
                      warna[index];
                      setState(() {});
                    },
                    child: Icon(Icons.wind_power),
                  ),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: Colors.teal,
                    ),
                    onPressed: () {
                      if (index == 0) {
                        index = warna.length - 1;
                        warna[index];
                      } else {
                        index--;
                        warna[index];
                      }
                      setState(() {});
                    },
                    child: Icon(Icons.skip_previous),
                  ),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: Colors.teal,
                    ),
                    onPressed: () {
                      if (index >= warna.length - 1) {
                        index = 0;
                        warna[index];
                      } else {
                        index++;
                        warna[index];
                      }
                      setState(() {});
                    },
                    child: Icon(Icons.skip_next),
                  ),
                ],
              ),
              SizedBox(height: 100),
            ],
          ),
        ),
      ),
    );
  }
}

class KotakWarna extends StatelessWidget {
  KotakWarna(this.warna);

  final Color warna;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 400,
      height: 300,
      decoration: BoxDecoration(
        color: warna,
        border: Border.all(
          color: Colors.teal,
        ),
      ),
    );
  }
}
