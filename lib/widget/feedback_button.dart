import 'package:flutter/material.dart';

class FeedbackButton extends StatelessWidget {
  const FeedbackButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () {
        showModalBottomSheet(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15),
          ),
          isDismissible: true,
          context: context,
          builder: (context) => Container(
            height: 1000,
            child: ListView(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 40, left: 40, right: 40),
                  child: RichText(
                    textAlign: TextAlign.justify,
                    text: TextSpan(
                      text: "MENGENAL ANHAR (Angka, Huruf, Warna) \n \n",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                      ),
                      children: [
                        TextSpan(
                            text:
                                "Aplikasi ini digunakan sebagai alat peraga bagi orang tua dalam memperkenalkan angka, huruf, dan warna kepada anak-anaknya. \n \n",
                            style: TextStyle(fontWeight: FontWeight.normal)),
                        TextSpan(
                            text:
                                "Jangan biarkan anak bermain HP sendiri, aplikasi ini semata-mata sebagai alat bantu bagi orang tua berinteraksi dengan anaknya. \n \nKritik dan saran silakan kirim melalui DM Instagram @fadlymahendra. \n \n",
                            style: TextStyle(fontWeight: FontWeight.normal)),
                      ],
                    ),
                  ),
                ),
                IconButton(
                  onPressed: () => Navigator.pop(context),
                  icon: Icon(Icons.close),
                  color: Colors.teal,
                ),
              ],
            ),
          ),
        );
      },
      icon: Icon(Icons.feedback),
      color: Colors.teal,
    );
  }
}
