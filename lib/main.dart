import 'package:anhar/pages/about_page.dart';
import 'package:anhar/pages/angka_page.dart';
import 'package:anhar/pages/huruf_page.dart';
import 'package:anhar/pages/warna_page.dart';
import './widget/feedback_button.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(Anhar());
}

class Anhar extends StatelessWidget {
  const Anhar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late int index;
  @override
  void initState() {
    index = 3;
    super.initState();
  }

  List showWidget = [
    Center(
      child: AngkaPage(),
    ),
    Center(
      child: HurufPage(),
    ),
    Center(
      child: WarnaPage(),
    ),
    Center(
      child: AboutPage(),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        // leading: Icon(Icons.family_restroom),
        actions: [FeedbackButton()],
        title: Text(
          "Mengenal Anhar",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.teal,
          ),
        ),
        centerTitle: false,
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      body: showWidget[index],
      bottomNavigationBar: BottomNavigationBar(
        elevation: 0,
        unselectedItemColor: Colors.teal,
        selectedItemColor: Colors.green,
        backgroundColor: Colors.white,
        currentIndex: index,
        iconSize: 26,
        showSelectedLabels: false,
        onTap: (value) {
          setState(() {
            index = value;
          });
        },
        items: [
          BottomNavigationBarItem(
            icon: Icon(
              Icons.onetwothree_outlined,
              size: 40,
            ),
            label: "Angka",
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.abc,
              size: 40,
            ),
            label: "Huruf",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.color_lens_outlined),
            label: "Warna",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: "About",
          ),
        ],
      ),
    );
  }
}
